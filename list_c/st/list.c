#include"list.h"
SLNode* LTBuyNode(LTDataType x)//申请一个新的节点
{
    SLNode* node = (SLNode*)malloc(sizeof(SLNode));
    node->prev = NULL;
    node->next = NULL;
    node->data = x;
    return node;
}

SLNode* LTInitNode()
{
    SLNode* phead = LTBuyNode(-1);//-1为无意义值
    phead->next = phead;//循环节点特性，只有一个节点做的特殊处理
    phead->prev = phead;
    return phead;
}

void LTPushFront(SLNode* phead,LTDataType x)
{
    assert(phead);//判断合法性
    SLNode* newnode = LTBuyNode(x);//申请节点
    SLNode* prenode = phead->next;//连接节点
    prenode->prev = newnode;
    newnode->next = prenode;
    phead->next = newnode;
    newnode->prev = phead;
}

void LTPrint(SLNode* phead)
{
    assert(phead);
    assert(phead->next != phead);
    SLNode* cur = phead->next;
    printf("guard<==>");
    while (cur!=phead)//链表结束标志
    {
        printf("%d<==>", cur->data);
        cur = cur->next;
    }
    printf("\n");
}

void LTPopFront(SLNode* phead)
{
    assert(phead);
    SLNode* cur = phead->next;//连接后序节点
    phead->next = cur->next;
    cur->next->prev = phead;
    free(cur);//释放头节点
}

void LTPushRear(SLNode* phead, LTDataType x)
{
    SLNode* newnode = LTBuyNode(x);
    SLNode* cur = phead->prev;
    newnode->prev = cur;
    cur->next = newnode;
    phead->prev = newnode;
    newnode->next = phead;
}

SLNode* Search(SLNode* phead, LTDataType x)
{
    assert(phead);
    assert(phead->next != phead);
    SLNode* cur = phead->next;
    while (cur!=phead)//结束标志
    {
        if (cur->data == x) 
        {
            return cur;
        }
        cur = cur->next;
    }
    return NULL;
}

void LTInsert(SLNode* SearchNode, LTDataType x)
{
    assert(SearchNode);
    SLNode* newnode = LTBuyNode(x);
    SLNode* cur = SearchNode->prev;
    cur->next = newnode;
    newnode->prev = cur;
    newnode->next = SearchNode;
    SearchNode->prev = newnode;
}

void LTErase(SLNode* NewNode)
{
    assert(NewNode);
    SLNode* prev = NewNode->prev;
    SLNode* next = NewNode->next;
    prev->next = next;
    next->prev = prev;
    free(NewNode);
}

void LTDestroy(SLNode* phead)
{
    assert(phead);
    assert(phead->next != phead);
    SLNode* cur = phead->next;
    while (cur != phead)
    {
        SLNode* tmp = cur->next;
        free(cur);
        cur = tmp;
    }
    free(phead);
}