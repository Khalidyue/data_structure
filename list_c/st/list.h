#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int LTDataType;

typedef struct ListNode
{
    /* data */
    struct ListNode* prev;
    struct ListNode* next;
    LTDataType data;
}SLNode;

SLNode* LTBuyNode(LTDataType x);

SLNode* LTInitNode();

void LTPushFront(SLNode* phead,LTDataType x);

void LTPrint(SLNode* phead);

void LTPopFront(SLNode* phead);

void LTPushRear(SLNode* phead, LTDataType x);

SLNode* Search(SLNode* phead, LTDataType x);

void LTInsert(SLNode*NewNode , LTDataType x);

void LTErase(SLNode* NewNode);

void LTDestroy(SLNode* phead);
