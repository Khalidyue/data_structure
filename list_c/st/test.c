#include"list.h"
void test1()
{
    SLNode* newphead = LTInitNode();
    LTPushFront(newphead,4);
    LTPushFront(newphead, 3);
    LTPushFront(newphead, 2);
    LTPushFront(newphead, 1);
    LTPopFront(newphead);
    LTPrint(newphead);
    LTPushRear(newphead, 9);
    LTPushRear(newphead, 8);
    LTPrint(newphead);
    SLNode*wantif= Search(newphead, 9);
    LTInsert(wantif, 6);
    LTPrint(newphead);
    LTErase(wantif);
    LTPrint(newphead);
    LTDestroy(newphead);
    free(newphead);
    LTPrint(newphead);
}
int main()
{
    test1();
    return 0;
}